**Integrantes:**

Nicolas Stiven Jaimes Duarte y Daniel Baez Acevedo

**Descripción del Proyecto:**

Nuestro proyecto de clase consiste en la elaboración de una aplicación web para una empresa de alimentos, la cual, ofrece los siguiente servicios:
- Menú de comida a lo largo del dia 
- Reservación de mesas
- Elaboración de tortas y ponques personalizadas para cualquier evento
- Alquiler y decoración del sitio de acuerdo al evento.
